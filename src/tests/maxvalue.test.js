const app = require('../app');
const {items, capacity} = require('./mocks/sampleItems');

// No edge case testing required since the specification indicate that values are strictly positive.
describe("knapsack optimization tests", () => {
  test("optimal value has been loaded", () => { 
    expect(app.knapsack(items[0], capacity[0])).toBe(90);
    expect(app.knapsack(items[1], capacity[1])).toBe(180);
    expect(app.knapsack(items[2], capacity[2])).toBe(220);
  });
});