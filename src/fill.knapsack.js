/**
 * this script is called as the entry point when running the module on terminal.
 */
const app = require('./app');
const {items, capacity} = require('./data/items');

console.log(app.knapsack(items, capacity));
