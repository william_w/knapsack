// Use the following variables to populate parameters for the knapsack problem

// items holds a list of item objects
exports.items = [ { "weight": 5, "value": 10 }, { "weight": 4, "value": 40 }, { "weight": 6, "value": 30 }, { "weight": 4, "value": 50 } ];

// capacity holds the maximum weight that the knapsack can hold
exports.capacity = 10;