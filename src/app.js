module.exports = {
  /**
   * This is the main function. It initializes a two dimensional array that we will use to memorize 
   * computations to avoid redundant recursive calls. It then calls the ks function which runs
   * recursively to compute our solution.
   * @param {Array} items
   * @param {number} capacity
   */
  knapsack: (items, capacity) => {
    let list = [...Array(items.length)].map(() => Array(capacity).fill(undefined));

   /**
   * The ks function will be called recursively until we have established the final result
   * @param {number} length
   * @param {number} capacity
   * NB: Since the array is 0-based, we will reduce the length by one when supplying the array index
   */
    const ks = (length, capacity) => {
      const i = length - 1;
      const C = capacity - 1;
      // Check if the ith item has been evaluated, if so return the memorized value
      if (list[i] && list[i][C] != undefined)
        return list[i][C];

      // This is the base check, it evaluates whether we have inserted all items in the knapsack or
      // the knapsack capacity has been depleted. If so, it returns 0 thereby ending the recusrsion
      if (length === 0 || capacity === 0)
        return 0;
      else if (items[i].weight > capacity) { // check if item weighs more than allowed capacity
        // If so, discard the item by reducing the item list by 1 (at the ith position) and call the 
        // ks function
        list[i][C] = ks(i, capacity);
        return list[i][C];
      }
      else { // condition is met if the ith item does not weigh more than the capacity
        // Return the maximum value of the two scenarios:
        // 1. the ith item is included in the knapsack
        // 2. the ith item is not included in the knapsack 
        const tmp1 = ks(i, capacity);
        const tmp2 = items[i].value + ks(i, capacity - items[i].weight);
        const result = Math.max(tmp1, tmp2);
        list[i][C] = result;
        return result;
      }
    }
    
    const optimizedValue = ks(items.length, capacity);
    return optimizedValue;
  }
}