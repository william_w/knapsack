# knapsack

Determine the maximum value of items that can be stored in a knapsack given a list of items and that the knapsack can only accommodate a weight of only 10 units.

## Pre-requisites
You will need to have the following installed and configured appropriately on your computer.
* Nodejs
* Git
* yarn


## Setup:

### Clone the project to your local drive
1. Navigate to your projects folder.
2. Run the command `git clone git@gitlab.com:william_w/knapsack.git` to clone the repository.

## Test and Run:

### Initialize your project
1. Open your project folder on your preferred terminal.
2. Run the command `yarn` to install the project dependencies.

    
### Running the project

1.  Run the command `yarn test` to execute all the unit tests.
2.  On your terminal, run the command `node fill.knapsack.js` to execute the application locally. 

## Assumptions:

1.  The challenge parameters including the item properties and maximum knapsack weight will be provided via a file or a request payload. To simulate this, we will use the `items.js` file located under `/src/data`.